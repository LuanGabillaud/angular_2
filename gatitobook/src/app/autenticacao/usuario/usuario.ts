export interface Usuario {
  id?: number;
  email?: string;
  name?: string;
}
