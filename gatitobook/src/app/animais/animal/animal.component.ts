import { Component, Input, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

const API = environment.apiURL;

@Component({
  selector: 'app-animal',
  templateUrl: './animal.component.html',
  styleUrls: ['./animal.component.css']
})
export class AnimalComponent implements OnInit {

  private urlOrinal = '';

  @Input() descricao = '';
  @Input() set url(url:string){
    if(url.startsWith('data')){
      this.urlOrinal = url;
    } else {
      this.urlOrinal = `${API}/imgs/${url}`;
    }
  }

  get url():string {
    return this.urlOrinal;
  }

  constructor() { }

  ngOnInit(): void {
  }

}
